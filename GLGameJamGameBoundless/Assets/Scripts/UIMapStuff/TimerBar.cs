﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBar : MonoBehaviour {
    public Image Topbar;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        Topbar.fillAmount = GameManager.instance.TimerCurrent / GameManager.instance.TimerMax;
	}
}
