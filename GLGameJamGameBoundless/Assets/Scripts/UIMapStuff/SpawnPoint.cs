﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour 
{

	// Use this for initialization
	void Start() 
	{
		GameManager.instance.Spawns.Add(this.gameObject);
	}
}
