﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMover : MonoBehaviour {

    public void MainMenu()
    {
        GameManager.instance.GameRunning = false;
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
    public void OptionsMenu()
    {
        GameManager.instance.GameRunning = false;
        SceneManager.LoadScene("Options", LoadSceneMode.Single);
    }

    public void LevelSelection()
    {
        GameManager.instance.Enemies.Clear();
		GameManager.instance.Spawns.Clear();
        GameManager.instance.net.Clear();
        GameManager.instance.TimerCurrent = GameManager.instance.TimerMax;
        GameManager.instance.Score = 0;
        GameManager.instance.GameRunning = true;
        SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
    }
    public void VictoryScene()
    {
        GameManager.instance.GameRunning = false;
        GameManager.instance.TimerCurrent = GameManager.instance.TimerMax;
       
        SceneManager.LoadScene("ScoreScreen", LoadSceneMode.Single);
    }
    public void DefeatScene()
    {
        GameManager.instance.GameRunning = false;
        GameManager.instance.TimerCurrent = GameManager.instance.TimerMax; 
        SceneManager.LoadScene("Defeat", LoadSceneMode.Single);
    }
    public void Tutorial()
    {
        GameManager.instance.GameRunning = false;
        SceneManager.LoadScene("TutorialScene", LoadSceneMode.Single);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
