﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Textureoffsetscrollingback : MonoBehaviour 
{
	public Vector2 offset;
	public Renderer rend;
	public Transform cameraTransform;
	// Use this for initialization
	void Start () 
	{
		cameraTransform = Camera.main.transform;
		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 offset3 = new Vector3(-cameraTransform.position.x, -cameraTransform.position.y, cameraTransform.position.z);
		offset = (Vector2)offset3;
		rend.material.SetTextureOffset("_MainTex", offset/50);
	}
}
