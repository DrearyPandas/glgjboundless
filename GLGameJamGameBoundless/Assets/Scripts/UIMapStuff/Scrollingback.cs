﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrollingback : MonoBehaviour 
{
	public float backgroundSize;
	public Transform cameraTransform;
	public Transform[] layers;
	public int upIndex;
	public int downIndex;
	public float viewZone = 10;
	public int leftIndex;
	public int rightIndex;
	public Transform[] uplayers;

	void Start () 
	{
		cameraTransform = Camera.main.transform;
		layers = new Transform[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			layers[i] = transform.GetChild(i);
			uplayers[i] = transform.GetChild(i);
		}
		leftIndex = 0;
		rightIndex = layers.Length - 1;
		upIndex = 0;
		downIndex = layers.Length - 1;
	}
	
	void Update()
	{
		if (cameraTransform.position.x < (layers[leftIndex].position.x + viewZone))
		{
			ScrollLeft();
		}
		if (cameraTransform.position.x > (layers[rightIndex].position.x - viewZone))
		{
			ScrollRight();
		}
		if (cameraTransform.position.y < (layers[upIndex].position.y + viewZone))
		{
			ScrollUp();
		}
		if (cameraTransform.position.y > (layers[downIndex].position.y - viewZone))
		{
			ScrollDown();
		}
	}
	
	void ScrollLeft()
	{
		float tempx = (layers[leftIndex].position.x - backgroundSize);
		float tempy = layers[rightIndex].position.y;
		float tempz = layers[rightIndex].position.z;
		
		layers[rightIndex].position = new Vector3(tempx, tempy, tempz);
		
		//layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - backgroundSize);
		leftIndex = rightIndex;
		rightIndex--;
		if (rightIndex < 0)
		{
			rightIndex = layers.Length - 1;
		}
	}
	
	
	void ScrollRight()
	{
		float tempx = (layers[rightIndex].position.x + backgroundSize);
		float tempy = layers[leftIndex].position.y;
		float tempz = layers[leftIndex].position.z;
		
		layers[leftIndex].position = new Vector3(tempx, tempy, tempz);
		
		//layers[leftIndex].position = Vector3.right * (layers[rightIndex].position.x + backgroundSize);
		rightIndex = leftIndex;
		leftIndex++;
		if (leftIndex == layers.Length)
		{
			leftIndex = 0;
		}
	}
	
	void ScrollUp()
	{
		float tempy = (layers[upIndex].position.y - backgroundSize);
		float tempx = layers[downIndex].position.x;
		float tempz = layers[downIndex].position.z;
	
		layers[downIndex].position = new Vector3(tempx, tempy, tempz);
		//layers[downIndex].position = Vector3.up * (layers[upIndex].position.y - backgroundSize);
		upIndex = downIndex;
		downIndex--;
		if (downIndex < 0)
		{
			downIndex = layers.Length - 1;
		}
	}
	
	
	void ScrollDown()
	{
		float tempy = (layers[downIndex].position.y + backgroundSize);
		float tempx = layers[upIndex].position.x;
		float tempz = layers[upIndex].position.z;
		
		layers[upIndex].position = new Vector3(tempx, tempy, tempz);
		//layers[upIndex].position = Vector3.up * (layers[downIndex].position.y + backgroundSize);
		downIndex = upIndex;
		upIndex++;
		if (upIndex == layers.Length)
		{
			upIndex = 0;
		}
	}
}
