﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
	private Transform tf;
	public Vector3 targetTF;
	
	// Use this for initialization
	void Start () 
	{
		tf = GetComponent<Transform>();
		targetTF = GameManager.instance.player.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		targetTF = (GameManager.instance.player.transform.position + new Vector3(0, 0, -10));
		tf.position = targetTF;
	}
}
