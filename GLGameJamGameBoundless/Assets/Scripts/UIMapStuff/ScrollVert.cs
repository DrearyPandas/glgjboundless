﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollVert : MonoBehaviour 
{
	public float backgroundSize;
	public Transform cameraTransform;
	public Transform[] layers;
	public float viewZone = 10;
	public int upIndex;
	public int downIndex;


	void Start () 
	{
		cameraTransform = Camera.main.transform;
		layers = new Transform[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			layers[i] = transform.GetChild(i);
		}
		upIndex = 0;
		downIndex = layers.Length - 1;
	}
	
	void Update()
	{
		if (cameraTransform.position.y < (layers[upIndex].position.y + viewZone))
		{
			ScrollUp();
		}
		if (cameraTransform.position.y > (layers[downIndex].position.y - viewZone))
		{
			ScrollDown();
		}
	}
	
	void ScrollUp()
	{
		float tempy = (layers[upIndex].position.y - backgroundSize);
		float tempx = layers[downIndex].position.x;
		float tempz = layers[downIndex].position.z;
	
		layers[downIndex].position = new Vector3(tempx, tempy, tempz);
		//layers[downIndex].position = Vector3.up * (layers[upIndex].position.y - backgroundSize);
		upIndex = downIndex;
		downIndex--;
		if (downIndex < 0)
		{
			downIndex = layers.Length - 1;
		}
	}
	
	
	void ScrollDown()
	{
		float tempy = (layers[downIndex].position.y + backgroundSize);
		float tempx = layers[upIndex].position.x;
		float tempz = layers[upIndex].position.z;
		
		layers[upIndex].position = new Vector3(tempx, tempy, tempz);
		//layers[upIndex].position = Vector3.up * (layers[downIndex].position.y + backgroundSize);
		downIndex = upIndex;
		upIndex++;
		if (upIndex == layers.Length)
		{
			upIndex = 0;
		}
	}

}