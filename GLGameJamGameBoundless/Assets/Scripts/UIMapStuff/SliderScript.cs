﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderScript : MonoBehaviour {

    public Slider MaxEnemiesSlider;
    public Slider MaxTimeSlider;

    public Text EnemiesCount;
    public Text TimeCount;

    // Use this for initialization
    void Start () {
        MaxTimeSlider.value = GameManager.instance.TimerMax ;
        MaxEnemiesSlider.value =GameManager.instance.maxEnemies;
    }
	
	// Update is called once per frame
	void Update () {
        GameManager.instance.TimerMax = MaxTimeSlider.value;
        GameManager.instance.maxEnemies = MaxEnemiesSlider.value;

        EnemiesCount.text = GameManager.instance.TimerMax.ToString();
        TimeCount.text = GameManager.instance.maxEnemies.ToString();
    }
   
}
