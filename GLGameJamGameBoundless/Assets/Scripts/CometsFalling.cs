﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CometsFalling : MonoBehaviour {
    public float spawnTime;
    public Transform comet;
    private bool startFalling;
    // Use this for initialization
    void Start() {
        startFalling = true;
        StartCoroutine(Comets());
     }
	// Update is called once per frame
	void Update () {
        spawnTime = Random.Range(1, 6);
    }
    IEnumerator Comets()
    {
        if (startFalling == true) {
            yield return new WaitForSeconds(spawnTime);
            Instantiate(comet, transform.position, transform.rotation);
            startFalling = false;
            yield return new WaitForSeconds(1);
            StartCoroutine(AgainAgain());
         }
    }
    IEnumerator AgainAgain()
    {
        yield return new WaitForSeconds(spawnTime);
        startFalling = true;
        StartCoroutine(Comets());
    }
}
