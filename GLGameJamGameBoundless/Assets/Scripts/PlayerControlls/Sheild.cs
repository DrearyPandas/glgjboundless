﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheild : MonoBehaviour {

    public Color color1;
    public Color color2;
    public Color color3;

    public SpriteRenderer Sr;

    // Use this for initialization
    void Start () {
        Sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.instance.playerHealth == 3)
        {
            Sr.color = color3;
        }
        if (GameManager.instance.playerHealth == 2)
        {
            Sr.color = color2;
        }
        if (GameManager.instance.playerHealth == 1)
        {
            Sr.color = color1;
        }

    }
}
