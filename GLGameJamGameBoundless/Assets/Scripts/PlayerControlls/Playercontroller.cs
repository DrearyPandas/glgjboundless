﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playercontroller : MonoBehaviour 
{
	public float moveSpeed;
	public float rotateSpeed;
	public Rigidbody2D rb;
	public Transform tf;
	public Animator anim;
	public bool isMoving;
	
	void Start ()
	{
		rb = GetComponent<Rigidbody2D>();
		tf = GetComponent<Transform>();
		anim = GetComponent<Animator>();
		isMoving = false;
		GameManager.instance.player = this.gameObject;
	}
	
	void FixedUpdate()
	{
		if (isMoving)
		{
			anim.Play("ShipMoving");
		}
		else
		{
			anim.Play("PlayerIdle");
		}
		if (Input.GetKey(KeyCode.W))
		{
			rb.AddForce(transform.up * (Time.deltaTime * moveSpeed));
			isMoving = true;
		
		}
		if (Input.GetKey(KeyCode.A))
		{
			tf.transform.Rotate((new Vector3 (0, 0, 1)) * (Time.deltaTime * rotateSpeed));
			isMoving = true;
		}
		if (Input.GetKey(KeyCode.D))
		{
			tf.transform.Rotate((new Vector3 (0, 0, 1)) * (Time.deltaTime * -rotateSpeed));
			isMoving = true;
		}
		if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
		{
			isMoving = false;
		}
	}
}
