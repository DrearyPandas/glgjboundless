﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour 
{
	void OnTriggerExit2D(Collider2D coll)
	{
		for (int i = 0; i < GameManager.instance.Enemies.Count; i++)
		{
			if(GameManager.instance.Enemies[i] == coll.gameObject)
			{
				GameManager.instance.Enemies.Remove(coll.gameObject);
				Destroy(coll.gameObject);
			}
		}
		
	}
}
