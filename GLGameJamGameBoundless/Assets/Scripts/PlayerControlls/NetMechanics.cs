﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetMechanics : MonoBehaviour {
    
    public int NumOfVerticies = 0;
    public GameObject net;
    [HideInInspector]
    public LineRenderer lr;
    public GameObject Line;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        NumOfVerticies = GameManager.instance.net.Count;

        if (Input.GetMouseButtonDown(0))
        {
            GameObject NewNet = Instantiate(net,GameManager.instance.player.transform.position, Quaternion.identity);
            GameManager.instance.net.Add(NewNet);
            

            if(NumOfVerticies > GameManager.instance.MaxNets)
            {
                GameObject DestroyNet = GameManager.instance.net[0];
                GameManager.instance.net.Remove(DestroyNet);
                Destroy(DestroyNet);
                
                
                GameObject DestroyLine = GameManager.instance.lines[0];
                GameManager.instance.lines.Remove(DestroyLine);
                Destroy(DestroyLine);
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            if(NumOfVerticies >= 3) {
                GameObject NetLine = Instantiate(Line, GameManager.instance.net[NumOfVerticies - 1].transform.position, Quaternion.identity);
                GameManager.instance.lines.Add(NetLine);

                lr = NetLine.GetComponent<LineRenderer>();

                Vector3 direction = GameManager.instance.net[0].transform.position - GameManager.instance.net[NumOfVerticies - 1].transform.position;
                lr.SetPosition(0, direction);

                GameManager.instance.Circleing();
            }  
        }
    }
}
