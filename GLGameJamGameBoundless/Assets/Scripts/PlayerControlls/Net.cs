﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Net : MonoBehaviour {
    public int NumInLine;
    public GameObject Line;
    public SpriteRenderer Sr;
    public LineRenderer lr;
    
  
    public List<Vector3> positions;

    // Use this for initialization
    void Start () {
       
       for (int i = 0; i < GameManager.instance.net.Count; i++)
       {
            if (GameManager.instance.net[i] == gameObject)
            {
                NumInLine = i;
            }
       }
       if (NumInLine > 0)
       {
            GameObject NetLine = Instantiate(Line, GameManager.instance.player.transform.position, Quaternion.identity);
            GameManager.instance.lines.Add(NetLine);

            positions.Add(GameManager.instance.net[NumInLine - 1].transform.position);
            positions.Add(GameManager.instance.net[NumInLine].transform.position);

            lr = NetLine.GetComponent<LineRenderer>();
            
            Vector3 direction = positions[0] - positions[1];
            lr.SetPosition(0, direction);

        }
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < GameManager.instance.net.Count; i++)
        {
            if (GameManager.instance.net[i] == gameObject)
            {
                NumInLine = i;
            }
        }
    }

    public void CaptureColor()
    {
        Sr = GetComponent <SpriteRenderer> ();

        Sr.color = GameManager.instance.CaptureColor;

    }
    public void MissColor()
    {
        Sr = GetComponent<SpriteRenderer>();

        Sr.color = GameManager.instance.MissColor;

    }
}
