﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetDestroyer : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Net")
        {
            Debug.Log("net out ofbounds");
            for (int i = GameManager.instance.net.Count - 1; i >= 0; i--)
            {
                GameObject DestroyNet = GameManager.instance.net[i];
                GameManager.instance.net.Remove(DestroyNet);
                Destroy(DestroyNet);
            }

            for (int i = GameManager.instance.lines.Count - 1; i >= 0; i--)
            {
                GameObject DestroyLine = GameManager.instance.lines[i];
                GameManager.instance.lines.Remove(DestroyLine);
                Destroy(DestroyLine);
            }
        }
    }
}
