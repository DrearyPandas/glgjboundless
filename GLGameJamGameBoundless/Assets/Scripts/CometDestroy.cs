﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CometDestroy : MonoBehaviour {
    public float destroyTimer;

	// Use this for initialization
	void Start () {
        StartCoroutine(DestroyComet());
		
	}
	IEnumerator DestroyComet()
    {
        yield return new WaitForSeconds(destroyTimer);
        Destroy(gameObject);
    }


}
