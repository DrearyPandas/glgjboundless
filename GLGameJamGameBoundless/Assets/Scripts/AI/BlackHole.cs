﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : EnemieParent {
    public Rigidbody2D rb;
    public Transform tf;
    public Transform EnemyTf;
    public float POWAR;
    public Vector3 Direction;

    void Start()
    {
		GameManager.instance.Enemies.Add(gameObject);
        tf = GetComponent<Transform>();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.playerHealth --;
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            rb = other.GetComponent<Rigidbody2D>();
            EnemyTf = other.GetComponent<Transform>();
            Direction = tf.position - EnemyTf.position;
            Direction.Normalize();
            
            rb.AddForce(Direction * POWAR);
        }

    }
}
