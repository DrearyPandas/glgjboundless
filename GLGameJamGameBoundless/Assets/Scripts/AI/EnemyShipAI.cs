﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipAI : EnemieParent
{	
	public float moveSpeed;
	public float rotateSpeed;
	public bool detectPlayer;
	public Transform tf;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () 
	{
		GameManager.instance.Enemies.Add(this.gameObject);
		tf = GetComponent<Transform>();
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(detectPlayer)
		{
			Vector3 relativePos = GameManager.instance.player.transform.position - tf.position;
			float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;
			tf.rotation = Quaternion.Lerp((this.gameObject.transform.rotation), Quaternion.Euler(0, 0, angle - 90), rotateSpeed);
			tf.position = tf.position + (transform.up * (Time.deltaTime * moveSpeed));
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.gameObject.name.Contains("Player"))
		{
			detectPlayer = true;
		}
	}
	
	void OnTriggerExit2D(Collider2D coll)
	{
		if(coll.gameObject.name.Contains("Player"))
		{
			detectPlayer = false;
		}
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.playerHealth--;
        }
    }
}
