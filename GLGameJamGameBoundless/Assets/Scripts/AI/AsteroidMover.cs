﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMover : EnemieParent{
    public float AsteroidSpeed;
    Vector3 direction;
    float RandX;
    float RandY;
	public float rotSpeed;

    [HideInInspector]
    public Transform tf;
    [HideInInspector]
    public Rigidbody2D rb;


	// Use this for initialization
	void Start () 
	{
		float i = Random.Range(0, 10);
		rotSpeed = (rotSpeed * i);
		
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        GameManager.instance.Enemies.Add(this.gameObject);

        float RandX = Random.Range(-1f, 1f);
        float RandY = Random.Range(-1f, 1f);

        direction = new Vector3(RandY, RandX, 0);
        direction.Normalize();

        rb.AddForce(direction * AsteroidSpeed);
		
    }
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate(0, 0, rotSpeed * Time.deltaTime);
	}

    void OnCollisionEnter2D(Collision2D other)
    {

    }
}
