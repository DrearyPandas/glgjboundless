﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoBehaviorEnemy : EnemieParent
{

	// Use this for initialization
	void Start () 
	{
		GameManager.instance.Enemies.Add(this.gameObject);
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player")
		{
			GameManager.instance.playerHealth --;
		}
	}
}
