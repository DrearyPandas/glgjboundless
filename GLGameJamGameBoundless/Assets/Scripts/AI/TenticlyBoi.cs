﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenticlyBoi : EnemieParent {

    [HideInInspector]
    public Transform tf;
    public Rigidbody2D rb;
    public Animator an;

    public bool isMoving;
    public Vector3 direction;
    public States currentState;

    public float MaxTime;
    public float CurrentTime = 0;
    public float POWER;
    public float rotateSpeed;

    public enum States
    {
        Random, AtPlayer
    }
	// Use this for initialization
	void Start () {
        an = GetComponent<Animator>();
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        currentState = States.Random;
        GameManager.instance.Enemies.Add(gameObject);
	}
	
	// Update is called once per frame
	void Update ()
    {
        CurrentTime =CurrentTime + Time.deltaTime;
        if (CurrentTime >= MaxTime)
        {
           if (currentState == States.Random)
           {
                random();
           }
           if (currentState == States.AtPlayer)
           {
                AtPlayer();
           }
           CurrentTime = 0;
        }
    }

    public void random()
    {
        float RandX = Random.Range(-1f, 1f);
        float RandY = Random.Range(-1f, 1f);

        direction = new Vector3(RandY, RandX, 0);
 
        direction.Normalize();
        rb.AddForce(direction * POWER);
        an.Play("SwimSquid");
        
    }

    public void AtPlayer()
    {
        direction = GameManager.instance.player.transform.position - tf.position;

        direction.Normalize();
        rb.AddForce(direction * POWER);
        an.Play("SwimSquid");
       
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            currentState = States.AtPlayer;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            currentState = States.Random;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.playerHealth--;
            Debug.Log("Heallth - 1");
        }
    }
}
