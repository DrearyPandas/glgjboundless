﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
    [HideInInspector]
    public static GameManager instance;
    [HideInInspector]
	public Playercontroller playerScript;
    public GameObject player;
    public GameObject SwirlyBoi;
    public List<GameObject> net;
    public List<GameObject> lines;
    public List<GameObject> Enemies;
	public List<GameObject> Spawns;
	public List<GameObject> enemyTypes;
    public List<GameObject> DestroyEnemies;
    public int MaxNets;
    public float CaptureWait;
    public Color CaptureColor;
    public Color MissColor;
    public float playerHealth = 3;

    [Range(5,30)]
	public float maxEnemies;

    public bool GameRunning;

    [Range(0,180)]
    public float TimerMax;
    public float TimerCurrent;

    public float Score;
    public float HighScore;

    


    // Use this for initialization
    void Awake () 
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
		DontDestroyOnLoad(this.gameObject);

	}
	
	// Update is called once per frame
	void Update () 
	{
		if(GameRunning == true)
        {
            TimerCurrent -= Time.deltaTime;

            if (Enemies.Count < maxEnemies)
            {
                int s = Random.Range(0, (Spawns.Count) - 1);
                int i = Random.Range(0, (enemyTypes.Count) - 1);
                GameObject spawn = Spawns[s];
                GameObject type = enemyTypes[i];
                Vector2 spawnPosition = new Vector2(spawn.transform.position.x, spawn.transform.position.y) + (Random.insideUnitCircle * 20);
                Instantiate(type, spawnPosition, Quaternion.identity);


            }
        }
      
        if (TimerCurrent <= 0)
        {
            TimerCurrent = TimerMax;
            GameRunning = false;
            playerHealth = 3;
            if (Score > HighScore)
            {
                HighScore = Score;
            }

            SceneManager.LoadScene("ScoreScreen", LoadSceneMode.Single);
            
        }

        if (playerHealth <=0)
        {
            GameRunning = false;
            TimerCurrent = TimerMax;
            SceneManager.LoadScene("Defeat", LoadSceneMode.Single);
            playerHealth = 3;
        }

    }

    public void Circleing()
    {
        int truecount = 0;
        
        for (int d = 0; d < Enemies.Count; d++)
        {
            bool t = IsPointInPolygon(Enemies[d].transform, net);
            Debug.Log(t);

            if (t == true)
            {
                Debug.Log(Enemies[d]);
                DestroyEnemies.Add(Enemies[d]);
                truecount++;
            }
        }


		if (truecount >= 1)
		{
				
			for (int i = 0; i < GameManager.instance.net.Count; i++)
			{
				Net Stuff = net[i].GetComponent<Net>();
				Stuff.CaptureColor();
			}
				
		}
		else 
		{
			for (int i = 0; i < GameManager.instance.net.Count; i++)
			{
				Net Stuff = net[i].GetComponent<Net>();
				Stuff.MissColor();
			}
		}

        for (int i = DestroyEnemies.Count-1; i >= 0; i--)
        {
            for(int u = 0; u < Enemies.Count; u++)
            {
                if(DestroyEnemies[i] == Enemies[u])
                {
                    Enemies.Remove(Enemies[u]);
                }
            }
            

            Debug.Log(DestroyEnemies.Count);
            Score = Score + DestroyEnemies[i].GetComponent<EnemieParent>().Score;
            GameObject temp = DestroyEnemies[i];
            Instantiate(SwirlyBoi, DestroyEnemies[i].transform.position, DestroyEnemies[i].transform.rotation);
            DestroyEnemies.Remove(DestroyEnemies[i]);
            Destroy(temp);
        }

        StartCoroutine(WaitTime());
		

    }
    IEnumerator WaitTime()
    {

        yield return new WaitForSeconds(CaptureWait);

        for (int i = GameManager.instance.net.Count - 1; i >= 0; i--)
        {
            GameObject DestroyNet = net[i];
            net.Remove(DestroyNet);
            Destroy(DestroyNet);
        }

        for (int i = GameManager.instance.lines.Count - 1; i >= 0; i--)
        {
            GameObject DestroyLine = lines[i];
            lines.Remove(DestroyLine);
            Destroy(DestroyLine);
        }
    }

    public bool IsPointInPolygon(Transform point, List<GameObject> polygon)
    {
        int polygonLength = polygon.Count, i = 0;
        bool inside = false;
        // x, y for tested point.
        float pointX = point.position.x, pointY = point.position.y;
        // start / end point for the current polygon segment.
        float startX, startY, endX, endY;
        Transform endPoint = (polygon[(polygonLength - 1)]).transform;
        endX = endPoint.position.x;
        endY = endPoint.position.y;
        while (i < polygonLength)
        {
            startX = endX; startY = endY;
            endPoint = (polygon[(i++)]).transform;
            endX = endPoint.position.x; endY = endPoint.position.y;
            //
            inside ^= (endY > pointY ^ startY > pointY) && ((pointX - endX) < (pointY - endY) * (startX - endX) / (startY - endY));
        }
        return inside;
    }
}
